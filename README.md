# api-testing

Environment to run defined PYTESTs via MQTT.

```
linux@offgrid:~/testing/api-testing$ docker-compose up
Starting mosquitto-api-tests ... done
Starting iqrfgd-api-tests    ... done
Recreating tavern-api-tests  ... done
Attaching to mosquitto-api-tests, iqrfgd-api-tests, tavern-api-tests
iqrfgd-api-tests | ============================================================================
iqrfgd-api-tests | DAEMON_VERSION="v2.1.0dev" BUILD_TIMESTAMP="2019-09-21T09:40:18"
iqrfgd-api-tests |
iqrfgd-api-tests | Copyright 2015 - 2017 MICRORISC s.r.o.
iqrfgd-api-tests | Copyright 2018 IQRF Tech s.r.o.
iqrfgd-api-tests | ============================================================================
iqrfgd-api-tests | startup ...
iqrfgd-api-tests | Running on Shape component system https://github.com/logimic/shape
iqrfgd-api-tests |
iqrfgd-api-tests | Launcher: Loading configuration file: configuration/config.json
iqrfgd-api-tests | Configuration directory set to: configuration
tavern-api-tests | ============================= test session starts ==============================
tavern-api-tests | platform linux -- Python 3.7.4, pytest-4.5.0, py-1.8.0, pluggy-0.13.0
tavern-api-tests | rootdir: /tests
tavern-api-tests | plugins: tavern-0.30.3
tavern-api-tests | collected 2 items
tavern-api-tests |
iqrfgd-api-tests | Loading cache ...
iqrfgd-api-tests | Loading cache success
tavern-api-tests | embed-coordinator/tests.tavern.yaml ..                                   [100%]
tavern-api-tests |
tavern-api-tests | ========================== 2 passed in 31.01 seconds ===========================
tavern-api-tests | ============================= test session starts ==============================
tavern-api-tests | platform linux -- Python 3.7.4, pytest-4.5.0, py-1.8.0, pluggy-0.13.0
tavern-api-tests | rootdir: /tests
tavern-api-tests | plugins: tavern-0.30.3
tavern-api-tests | collected 1 item
tavern-api-tests |
tavern-api-tests | embed-leds/tests.tavern.yaml .                                           [100%]
tavern-api-tests |
tavern-api-tests | ========================== 1 passed in 30.45 seconds ===========================
tavern-api-tests exited with code 0
```
