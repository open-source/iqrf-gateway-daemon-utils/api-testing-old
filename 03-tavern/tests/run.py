# It runs all tests.

from tavern.core import run

failure = run('embed-coordinator/tests.tavern.yaml', pytest_args=["-x"])
failure |= run('embed-leds/tests.tavern.yaml', pytest_args=["-x"])

if failure:
    print("Error running tests.")
